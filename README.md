# Prisma Ubuntu Station

## Journal de prise en main de la première machine test

Ci-dessous, la liste des actions que j'ai réalisé lorsque j'ai reçu une 1ere machine Ubuntu en test pour les développeurs IT Digital de Prisma

 - Définition du mot de passe à la remise en main de la machine au Tech Bar
 - Vérification de l'accès `sudo`
 - Test de la connexion au VPN OK (si FortiClient ne se lance pas au démarrage, chercher "FortiClient" dans les applications)
 - Mise à jour de la configuration de connexion "VPNSSL_Prisma" dans FortiClient :
   - Save login
   - Do not Warn Invalid Server Certificate
 - `apt install curl`
 - certificat SSL Prisma KO (pas d'accès à l'intranet Prisma sur Firefox, Chrome ou cURL)
 - changement dispo clavier (qwerty-fr) sur la session user (pas d'impact sur la mire de connexion)
 - redemarrage puis crash de /usr/libexec/gnome-session-binary (ne s'est jamais reproduit)
 - `apt install xournal` (pour ajouter une signature et annoter les PDF)
 - crash de "masvc self-strart" (SIGSEGV in tcache_get()). S'est reproduit à chaque démarrage pendant environ une semaine. Plus depuis.
 - `sudo cp *.crt /usr/local/share/ca-certificates/` (attention le certificat avait été pré-installé dans /usr/local/share/ca-certificate sans le S à la fin)
 - `sudo update-ca-certificates --fresh`
 - `certutil -d sql:$HOME/.pki/nssdb -A -t "C,C,C" -n prisma_root -i <nom-du-fichier>.crt`
 - Installation de Brew Linux
 - Ajout de `eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"` dans ~/.profile
 - Ajout de `export CURL_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt` dans ~/.profile
 - Installation de Visual Studio Code via `snap`
 - Mise à jour de /etc/krb5.conf pour mettre (attention au majuscules !) :
```
[libdefaults]
        default_realm = PRISMA-PRESSE.COM
```
 - Changement de mon mot de passe Active Directory :
```console
$ kinit
$ smbpasswd -U $USER -r prisma-presse.com
Old SMB password:
New SMB password:
Retype new SMB password:
Password changed for user nmoreau
```
